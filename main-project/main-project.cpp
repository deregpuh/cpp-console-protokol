#include <iostream>
#include <iomanip>
using namespace std;

#include "protokol_subscription.h"
#include "file_reader.h"
#include "constants.h"
int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �5. ��������\n";
    cout << "�����: ������ ����������\n";
    cout << "������: 16\n";
    protokol_subscription* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    int N;
    try
    {
        read("data.txt", subscriptions, size);
        for (int i = 0; i < size; i++)
        {
            //*****����� ���� �������� �����*******
            //����� ����
            cout << "���� �����: " << setw(2) << setfill('0') << subscriptions[i]->start.hour << ':';
            //����� �����
            cout << setw(2) << setfill('0') << subscriptions[i]->start.minute << ':';
            //����� ������
            cout << setw(2) << setfill('0') << subscriptions[i]->start.second << '\n';
            //********����� ������� ������*******
             //����� ����
            cout << "���� ������: " << setw(2) << setfill('0') << subscriptions[i]->finish.hour << ':';
            //����� �����
            cout << setw(2) << setfill('0') << subscriptions[i]->finish.minute << ':';
            //����� ������
            cout << setw(2) << setfill('0') << subscriptions[i]->finish.second << '\n';

            //����� ������� ���������� ����
            cout << "������ ���������� ����: " << subscriptions[i]->razmer1 << '\n';
            //����� ������� ������� ����
            cout << "������� ������� ����: " << subscriptions[i]->razmer2 << '\n';
            //����� �������� ���������  
            cout << "�������e ���������:  " << subscriptions[i]->title << '\n';

            cout << '\n';
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
  
    for (int i = 0; i < size; i++)
    {
        delete subscriptions[i];
    }
    return 0;
}
