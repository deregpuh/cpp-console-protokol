#ifndef FILE_READER_H
#define FILE_READER_H

#include "protokol_subscription.h"

void read(const char* file_name, protokol_subscription* array[], int& size);

#endif