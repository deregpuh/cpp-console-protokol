#ifndef BOOK_SUBSCRIPTION_H
#define BOOK_SUBSCRIPTION_H

#include "constants.h"

struct vremya
{
    int hour;
    int minute;
    int second;
};

struct program
{
    char title[MAX_STRING_SIZE];
   
};

struct protokol_subscription
{
  
    vremya  start;
    vremya finish;
    int razmer1;
    int razmer2;
 
    char title[MAX_STRING_SIZE];
};

#endif